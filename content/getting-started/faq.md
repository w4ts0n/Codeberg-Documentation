---
eleventyNavigation:
  key: FAQ
  title: Frequently Asked Questions
  parent: GettingStarted
  order: 80
---

## Is Codeberg well funded?
Codeberg is primarily funded by [donations](https://liberapay.com/codeberg). As of July 2020, with all expenses frozen, we have a runway of ~12 years, so you don't have to worry that our service will suddenly disappear.
Still, we can always make good use of donations, they will be used to build new features and extend our services.

## Where is Codeberg hosted?
We are hosted in Germany by [netcup GmbH](https://www.netcup.de/), as well as on our own hardware in a rented facility in Berlin, Germany.

## Is it allowed to host non-free software?
Our mission is to support the creation and development of Free Software; therefore we only allow repos licensed under an OSI/FSF-approved license. For more details see [Licensing article](/getting-started/licensing). However, we sometimes tolerate repositories that aren't perfectly licensed and focus on spreading awareness on the topic of improper FLOSS licensing and its issues.

## Why am I not allowed to set up an automatic mirror?
Automatically updating mirrors have been problematic for Codeberg. They can be created easily and - as people tend to not delete their stuff when leaving - consume an increasing amount of resources (traffic, disk space) over time.

If you have the need you can create manual mirrors by adding multiple remotes to your local repository and push the changes to your mirror with `git push --mirror`.

Some additional information can be found in [this blog post](https://blog.codeberg.org/mirror-repos-easily-created-consuming-resources-forever.html).

## Can I host private (non-licensed) repositories?
Codeberg is intended for free and open source content. However, as per our [Terms of Service](https://codeberg.org/codeberg/org/src/TermsOfUse.md#2-allowed-content-usage),

> Reasonable exceptions are to a very limited extent considered acceptable. For example, releasing single logo image files of a FLOSS project under no licence or a separate non-free licence that requires derivative works to use their own logo that is clearly distinguishable from the original work even in absence of trademark registration. Private repositories are only allowed for things required for FLOSS projects, like storing secrets, team-internal discussions or hiding projects from the public until they're ready for usage and/or contribution. They are also allowed for really small & personal stuff like your journal, config files, ideas or notes, but explicitly not as a personal cloud or media storage.

Since this is not what Codeberg *is meant for* in a more narrow sense, stricter limitations might be implemented in the future.

## What is the size limit for my repositories?
There is no fixed limit, but use cases that harm other users and projects due to excessive resource impact will get restricted. Please refer to our [Terms of Service](https://codeberg.org/codeberg/org/src/TermsOfUse.md#repositories-wikis-and-issue-trackers).

## What is the size limit for my avatar?
You can upload avatar pictures of up to 1 megabyte and 1024x1024 resolution.

## Is Codeberg open-source?
Codeberg is built on [Gitea](https://github.com/go-gitea/gitea), which is open-source. We make all of our changes and other code available under the [Codeberg organization](https://codeberg.org/Codeberg).

## What version of Gitea is Codeberg currently running?
You can check the version of Gitea that Codeberg uses through the [API here](https://codeberg.org/api/v1/version).

You will get a response like this: `{"version":"1.12.3+20-gb49f2abc5"}`. Here, 1.12.3 is the Gitea version number, and 20 is the number of patches applied on top of the release (which includes upstream commits and patches by Codeberg), with b49f2abc5 being the commit ID of the last patch applied on [the Codeberg branch](https://codeberg.org/Codeberg/gitea/commits/branch/codeberg) of Codeberg's Gitea repository. Note that the commit ID is without the leading "g".
