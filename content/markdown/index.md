---
eleventyNavigation:
  key: Markdown
  title: Writing in Markdown
  icon: pen-nib
  order: 40
---

On these pages, you will learn how to use Markdown in issues, texts and articles on Codeberg.

The Codeberg platform (based on [Gitea](https://gitea.io/)) uses Markdown as markup language for text formatting.
Gitea uses [Goldmark](https://github.com/yuin/goldmark) as rendering engine which is compliant with [CommonMark 0.30](https://spec.commonmark.org/0.30/).
The documentation of Codeberg is rendered using [markdown-it](https://github.com/markdown-it/markdown-it) which also support CommonMark.

## Further reading

You can read more about markdown in the following articles.
Additionally there are a lot of articles on the internet introducing Markdown. Just use the search engine of your choice to
look them up and learn more about Markdown.

- [A strongly defined, highly compatible specification of Markdown](https://commonmark.org/)
- [English Wikipedia article on Markdown](https://en.wikipedia.org/wiki/Markdown)
- [The Markdown Guide](https://www.markdownguide.org/)

