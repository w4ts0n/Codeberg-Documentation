---
eleventyNavigation:
key: MarkdownStyleguide
title: Markdown Styleguide
parent: Markdown
order: 10
---

This document should guide you to a use of the Markdown format which is commonly used in Codeberg.

## Bold

Use two stars at the beginning and the end of a section to highlight the **section in bold**.

## Italics

Use one star at the beginning and the end of a section to highlight the *section in italics*.

## Links

Use `[link description](link)` to link to another section, article or website. 

To link to an url without any Link-Description, surround the link by less-than `<` and
greater-than `>` characters. This is preferred to just adding an url within the text as it
is easier to parse the marked up urls.

Example:

<https://codeberg.org/>

## Topics

Use ATX-Style topics by adding one or more hash `#` signs to the start of the topic line.

## Preformatted sections

Use a single backtick character to preformat a word or a section within a line.

Use triple backticks to begin and end a preformatted section.

Use rendering hints to tell the renderer whether to syntax highlight your section and which language should be used.

## Tables

Always delimit both sides of a table with pipes `|`. Keep the tables readable even in the un-rendered text-form.