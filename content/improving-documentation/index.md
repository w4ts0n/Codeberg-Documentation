---
eleventyNavigation:
  key: ImprovingTheDocumentation
  title: Improving the Documentation
  icon: book
  order: 76
---

We're very happy you're considering to contribute to Codeberg Documentation!

### How do I contribute to Codeberg Documentation?

To contribute to Codeberg Documentation, the first thing you should do (if you haven't already done it) is to create your own fork of the [Codeberg/Documentation repository](https://codeberg.org/Codeberg/Documentation).

Then, for each major contribution that you want to make (e.g. for each new article), create a new branch, make your contributions and finally make a Pull Request to the Codeberg/Documentation repository. You can find the source code to all articles in the `content` directory of Codeberg Documentation's source tree.

The Codeberg Documentation collaborators will then review your pull request, they may request some changes and eventually, once all is good, they may merge your contribution into the official repository and deploy it to the live site.

