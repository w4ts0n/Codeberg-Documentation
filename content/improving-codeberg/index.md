---
eleventyNavigation:
  key: ImprovingCodeberg
  title: Improving Codeberg
  icon: hands-helping
  order: 75
---

Thank you for your interest in improving Codeberg!

Every helping hand, every contribution and every donation is warmly welcome.

You can support Codeberg in various ways:

### Use Codeberg

We're happy about every free software project actively using Codeberg,
because we believe that every free project deserves a free home.

We welcome free software projects that consider moving away from one of the
commercial software forges, such as GitHub, GitLab or Bitbucket.

By joining and using Codeberg, you're already helping our mission a lot - thank you!

And as always: Spread the word, tell your friends and invite them to collaborate on Codeberg with you :-)

### Donate to Codeberg

You can [donate to Codeberg e.V. using Liberapay](https://liberapay.com/codeberg/donate)
or [by SEPA wire transfer](https://codeberg.org/codeberg/org/src/Imprint.md#user-content-sepa-iban-for-donations).

Donations not only allow us to operate and scale the infrastructure behind Codeberg, but to develop the products that make Codeberg possible.
Codeberg is not an arbitrary Gitea instance, but commits itself into actively developing the software ecosystem around.

Codeberg e.V. is the non-profit organization behind Codeberg.org.

It's based in Berlin, Germany and [recognized as tax-exempt](https://codeberg.org/codeberg/org/src/Imprint.md#user-content-gemeinn%C3%BCtzigkeit-recognition-of-status-as-non-profit-ngo-recognition-as-tax-excempt-entity) by the German authorities.

### Join Codeberg e.V.

If you're interested in committing even more to Codeberg, consider [joining Codeberg e.V.](https://join.codeberg.org), the non-profit association behind Codeberg.org.

### Contribute to Codeberg

Do you have ideas on improving Codeberg? Have you found a bug and would like to report (or even fix) it? You're welcome to contribute to Codeberg, for example by [creating or commenting on an Issue](https://codeberg.org/Codeberg/Community/issues) or by [writing a
pull request](/collaborating/pull-requests-and-git-flow) to one of Codeberg's projects.

If you are not fond of doing codework, feel free to step up for some maintenance roles like Community Support, Social Media and more. Just drop us an email, or consider [Joining Codeberg](#join-codeberg-e.v.) where you'll get easier access to internal teams and task forces. Also, you can always [work on the Codeberg Docs](/improving-documentation/docs-contributor-faq).

Codeberg explicitly welcomes newcomers or career changers to its repos, and we will gladly mentor you as resources permit.
If you have questions, always feel free to ask in the Issue Trackers or on the Matrix Channels mentioned on the [Contact page](/contact).
Even tiny patches or suggestions, even if you are not a skilled developer, will be considered and are part of the community-maintenance mission of Codeberg.

You can read more about the code contribution workflow in [this article](contributing-code).

### Codeberg projects

<table class="table">
  <thead>
    <tr>
    	<th colspan="5">Project Overview</th>
    </tr>
    <tr>
        <th>Project</th>
        <th>Type</th>
        <th>Frontend/Backend</th>
        <th>Language</th>
        <th>Maintained by</th>
    </tr>
  </thead>
  <tbody>
    <tr>
		<th><a href="https://codeberg.org/Codeberg/gitea" >Gitea</a></th>
        <td>Main Project</td>
        <td>Fullstack</td>
        <td>Go, Less, JavaScript</td>
        <td>Codeberg*/Gitea</td>
    </tr>
    <tr>
        <th><a href="https://codeberg.org/Codeberg-CI/woodpecker">Woodpecker CI</a></th>
        <td>Main Project</td>
        <td>Fullstack</td>
        <td>Go, VueJS</td>
        <td>Codeberg*/Woodpecker-CI</td>
    </tr>
    <tr>
        <th><a href="https://codeberg.org/Codeberg/pages-server">Pages Server</a></th>
        <td>Main Project</td>
        <td>Backend</td>
        <td>Go</td>
        <td>Codeberg</td>
    </tr>
    <tr>
        <th><a href="https://codeberg.org/Codeberg/moderation">Moderation Tool</a></th>
        <td>Main Project</td>
        <td>Fullstack</td>
        <td>Go, HTML, JavaScript</td>
        <td>Codeberg</td>
    </tr>
    <tr>
        <th><a href="https://codeberg.org/Codeberg/Design">Codeberg Design</a></th>
        <td>Public Relations</td>
        <td>Frontend</td>
        <td>HTML, Less, JS, VueJS</td>
        <td>Codeberg</td>
    </tr>
    <tr>
        <th><a href="https://codeberg.org/Codeberg/Documentation">Codeberg Documentation</a></th>
        <td>Main Project</td>
        <td>Frontend</td>
        <td>Markdown</td>
        <td>Codeberg</td>
    </tr>
    <tr>
        <th><a href="https://codeberg.org/Codeberg/blog">Codeberg Blog</a></th>
        <td>Side Project</td>
        <td>Frontend</td>
        <td>Markdown</td>
        <td>Codeberg</td>
    </tr>
    <tr>
        <th><a href="https://codeberg.org/Codeberg/GetItOnCodeberg">GetItOnCodeberg</a></th>
        <td>Side Project</td>
        <td>Frontend</td>
        <td>HTML</td>
        <td>Codeberg</td>
    </tr>
    <!--
    <tr>
        <th><a href=""></a></th>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    -->
  </tbody>
</table>

\*This is only a fork for codeberg specific patches - PRs are accepted but they are prefered upstream
