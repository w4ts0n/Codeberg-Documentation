---
eleventyNavigation:
  key: Contact
  title: Contact
  icon: envelope
  order: 90
---

## Questions and Issues
The most important place to ask for help and report any issues with Codeberg is the [Codeberg/Community issue tracker](https://codeberg.org/Codeberg/Community/issues). It's never wrong to open a ticket there.

If you need quicker help or want to freely discuss topics, or follow up on Codeberg news, you can
- join the community on [Matrix](https://matrix.to/#/#codeberg-space:matrix.org)! General discussions take place in the [Codeberg room](https://matrix.to/#/#codeberg.org:matrix.org)
- follow us on [Mastodon](https://mastodon.technology/@codeberg) or if you aren't on the free and federated social network yet, [on Twitter](https://twitter.com/codeberg_org). You can also send us a direct message (Mastodon / Fediverse will have quicker response times, though).
- join the unofficial (community-created) `#codeberg` IRC channel at [libera.chat](https://libera.chat)
- use the unofficial (community-created) [Codeberg Subreddit](https://www.reddit.com/r/Codeberg/)

## Email
For user support on Codeberg.org that *can not be handled by the channels mentioned above* (e.g. because it requires sensitive information), you can send an email to [help@codeberg.org](help-PLEASE-REMOVE-THIS-FOR-SPAM-PROTECTION@codeberg.org).  
If you need to get in touch with the people behind the non-profit Codeberg e.V. (e.g. for managing your membership etc), please instead write to [codeberg@codeberg.org](mailto:codeberg-PLEASE-REMOVE-THIS-FOR-SPAM-PROTECTION@codeberg.org).

## Legal inquiries
For legal inquiries, please refer to the [Imprint](https://codeberg.org/codeberg/org/src/Imprint.md).

### Abuse

If you notice some unwanted content on Codeberg.org, please immediately report this to us, describing briefly why you think this should be removed.
Please note that **Codeberg is a platform for software development**, and we are **only responsible for content on Codeberg.org**, codeberg.page, codeberg-test.org and codeberg.eu.
If you visited Codeberg from another domain, this is likely because the software itself is developed at Codeberg.
Please reach out to the corresponding developer or to the operator of the platform directly.

The preferred way to contact us about abuse is by writing an email to [abuse@codeberg.org](mailto:abuse@codeberg.org).
If you prefer, you can also reach out via Mastodon or the Matrix chat, but if you have the time to get to an email client, we'd really appreciate that.
